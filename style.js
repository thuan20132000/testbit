
$('document').ready(function(){
    $('.slider-nav-slick').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1250,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
     
});

// ScrollTop Menu

$('function').ready(function(){

    $(window).scroll(function() {
        var $heightScroll = $(window).scrollTop();
        var $navbar = $('#navbar-expand-header');
        var btnToTop = $('#button-to-top');

      if($heightScroll > 50) 
      {
           $navbar.css('background-color','#333');
           $navbar.css('margin-top','0');   
           $navbar.css('padding','10px');
           btnToTop.css('display','block');
           
          $('#navbar-expand-header').css('top','0');
      } 
      else 
      {
        $navbar.css('background-color','');
        $navbar.css('margin-top','30px');
        btnToTop.css('display','none');
        $('#navbar-expand-header').css('top','13px');
      }
    });
    $('#button-to-top').on('click',function(){
          window.scrollTo(0,10);
    });


    var modal = $('#modal-image-list');
    $('.myImg').on('click', function(event){
         modal.css("display","block");
        $('#modal-display').attr('src',$(this).attr('src'));
    });

    window.onclick = function(event) {
     
      if (event.target == modal) {
        modal.children().css('display','none');
      }
    }

    $('.close-modal').on('click',function(){
        $('#modal-image-list').css('display','none');
    })
    $('.middle').on('click', function(event){
      $('#modal-image-list').css("display","block");
     $('#modal-display').attr('src',$(this).siblings('.myImg').attr('src'));  
      });

  

});


// slide menu
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
   
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      console.log(this.hash);
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
});


// button character slick slider


$('document').ready(function(){
  
  var i=1;
  $('.menu-icon').on('click',function(){
      if(i==1){
        $('.menu-item ul').css('display','block');
          i=0;
      }else{
        $('.menu-item ul').css('display','none');
        i=1;
      }
  });

  
    // $(window).resize(function(){
    //   var windowWidth = $(window).width();
    //   if(windowWidth > 1025){
    //     $('.menu-item ul').css('display','block');
    // //   }
      
    // });


    

});

